<?php

/**
 * @file
 * Connects your Drupal site to a Dropfort instance to track status and updates.
 */

use GuzzleHttp\Exception\RequestException;
use Drupal\Component\Utility\UrlHelper;

/**
 * Implements hook_cron().
 */
function dropfort_update_cron() {
  $time = time();

  if (($time - \Drupal::state()->get('dropfort_update.last_status', 0)) > 3600) {
    dropfort_update_send_status();
  }
}

/**
 * Send updates and status data to Dropfort.
 *
 * @param array $options
 *   Options to pass to the status method.
 *   - (string) site_key  : The site key to use during the HTTP request.
 *
 * @return mixed
 *   Return the HTTP response object, FALSE otherwise.
 *
 * @see _dropfort_update_system_status()
 */
function dropfort_update_send_status(array $options = NULL) {
  $config = \Drupal::config('dropfort_update.settings');
  $site_key = $config->get('site_key', '');
  $site_token = $config->get('site_token', '');
  $dropfort_base_url = $config->get('dropfort_url');

  // Skip the operation if the configuration isn't setup.
  if (empty($site_token) || empty($site_key) || empty($dropfort_base_url)) {
    return;
  }

  $data = [
    'site_key' => $site_key,
    'site_token' => $site_token,
    'site_status' => _dropfort_update_system_status(),
    'update_status' => update_calculate_project_data(update_get_available()),
  ];

  // If there was no data available, try again and force refresh
  // the update cache.
  if (empty($data['update_status'])) {
    $data['update_status'] = update_calculate_project_data(update_get_available(TRUE));
  }

  // Build HTTP request to retrieve filtered status list.
  $options = [
    'headers' => [
      'Content-Type' => 'application/json',
      'Accept' => 'application/json',
    ],
    'body' => json_encode($data),
    'method' => 'POST',
    'max_redirects' => 3,
    // We set a lower timeout since we don't want the user
    // waiting for the page too long.
    'timeout' => 10,
  ];

  // Check the URL before making the request.
  UrlHelper::setAllowedProtocols(['https']);
  $status_url = UrlHelper::filterBadProtocol($dropfort_base_url . '/api/v1/site/' . $site_key . '/status');

  // If the URL is invalid, return.
  if (!UrlHelper::isValid($status_url)) {
    \Drupal::logger('dropfort_update')->warning('Invalid status URL. Unable to send updates.');
    return;
  }

  try {
    $client = \Drupal::httpClient();
    $request = $client->request('POST', $status_url, $options);
    \Drupal::state()->set('dropfort_update.last_status', time());

    return $request;
  }
  catch (RequestException $e) {
    // Log the error.
    watchdog_exception('dropfort_update', $e);
    \Drupal::state()->set('dropfort_update.last_status', FALSE);
  }

  return FALSE;
}

/**
 * Implements hook_modules_installed().
 */
function dropfort_update_modules_installed($modules) {
  dropfort_update_send_status();
}

/**
 * Implements hook_modules_uninstalled().
 */
function dropfort_update_modules_uninstalled($modules) {
  dropfort_update_send_status();
}

/**
 * System status reporting.
 */
function _dropfort_update_system_status() {
  $system_manager = \Drupal::service('system.manager');
  $requirements = $system_manager->listRequirements();

  return $requirements;
}
