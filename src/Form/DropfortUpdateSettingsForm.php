<?php

namespace Drupal\dropfort_update\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Dropfort Update admin settings form.
 */
class DropfortUpdateSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'dropfort_update.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'dropfort_update_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('dropfort_update.settings');

    $form['site_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site Key'),
      '#size' => 70,
      '#required' => TRUE,
      '#default_value' => $config->get('site_key'),
      '#description' => $this->t('The site key provided in the Dropfort site interface.'),
    ];

    $form['site_token'] = [
      '#type' => 'password',
      '#title' => $this->t('Status Auth Token'),
      '#size' => 70,
      '#default_value' => $config->get('site_token'),
      '#description' => $this->t('The unqiue authentication token provided in the Dropfort site interface. Leave blank to keep existing value.'),
    ];

    $form['dropfort_url'] = [
      '#type' => 'url',
      '#title' => $this->t('Dropfort URL'),
      '#default_value' => $config->get('dropfort_url'),
      '#required' => TRUE,
      '#size' => 70,
      '#description' => $this->t('The url to the Dropfort instance. Default is https://api.dropfort.com.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    // Set the values.
    $this->config('dropfort_update.settings')
      ->set('site_key', $form_state->getValue('site_key'))
      ->set('dropfort_url', $form_state->getValue('dropfort_url'));

    if (!empty($form_state->getValue('site_token'))) {
      $this->config('dropfort_update.settings')->set('site_token', $form_state->getValue('site_token'));
    }

    $this->config('dropfort_update.settings')->save();

    // Send the current status.
    dropfort_update_send_status();
  }

}
