# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [2.0.3](https://git.dropfort.com///compare/v2.0.2...v2.0.3) (2022-11-23)

### [2.0.2](https://git.dropfort.com///compare/v2.0.1...v2.0.2) (2022-11-23)


### Bug Fixes

* **composer:** allow newer versions ([813c8e2](https://git.dropfort.com///commit/813c8e2e3fcda6adecada78b8683c7acc8a0fef5))

### 2.0.1 (2021-12-08)


### Features

* **admin:** Connection test ([3b59688](https://git.dropfort.com///commit/3b59688298b7993c6d42d0fd3eb04051cb769962))
* **ci:** add ci ([eb27b4b](https://git.dropfort.com///commit/eb27b4bbb3c60d85b9f5e1f4fa6bd244d5757591))
* **drush:** Dropfort download method ([4b146d1](https://git.dropfort.com///commit/4b146d13d5f768f8744ae94a6f6587c5bd695aae))
* **form:** add base settings form ([f66f767](https://git.dropfort.com///commit/f66f767ed3404c711f4b278be20e78e42bd20a59))
* **keys:** Add multiple keys support ([edd086d](https://git.dropfort.com///commit/edd086d05ea67e2d5b07b22d316d15b265e24596))
* **post update:** Add drush post update ([830cca1](https://git.dropfort.com///commit/830cca17b151b51c413cdb14a066d318c2023793))
* **prepopulate:** Change registration link ([e20b5ee](https://git.dropfort.com///commit/e20b5eeb829cb52e57c3aabfc089f602ea121146))
* **requirements:** Add connection requirement ([25d55a9](https://git.dropfort.com///commit/25d55a9ba4b13daf95480f30196f6e4c536d1e4a))
* **rest:** add site key to payload ([b763694](https://git.dropfort.com///commit/b76369491a0394036a32a87ce032d5795e9351c4))
* **schedule:** Set update checks for Wednesdays ([cbde346](https://git.dropfort.com///commit/cbde3467ae9a21d49f9ced15f5f75dceb4ac5044)), closes [#2455553](https://git.dropfort.com///issues/2455553)
* **site_key:** First working version of multi site key ([0eff399](https://git.dropfort.com///commit/0eff399301766efa93087e869382ced6268f297a))
* **updatecode:** Add listener on pm-updatecode ([fd60ecd](https://git.dropfort.com///commit/fd60ecd07aab099131ccf0cac6804b13a844bbae))
* **url:** add url settings option ([e885510](https://git.dropfort.com///commit/e885510a62a4fd8121f6ce20d409acfd55b3cb87))


### Bug Fixes

* **cache:** Don't cache empty project array ([b27fccf](https://git.dropfort.com///commit/b27fccfb78ae2768750a66dc3839b56d9e4774f6))
* **ci:** adjust paths ([3b4f2a3](https://git.dropfort.com///commit/3b4f2a35fc4d42898e4739ecd37e1b9dd712a9aa))
* **code:** remove unused var ([c0b0189](https://git.dropfort.com///commit/c0b0189a4dffded8562197422bbc83aaee32a87a))
* **code format:** various code format fixes ([319872e](https://git.dropfort.com///commit/319872e9cd281619776e0ff7922f040d2bc94ca0))
* **coding standards:** lots of general fixes to code format ([c48b9b0](https://git.dropfort.com///commit/c48b9b0aa04036807cc5746b0621cf274724b7fb))
* **comments:** Remove commented code ([45a2179](https://git.dropfort.com///commit/45a2179d1543992ba1c2d5a6ffdec00ed0743500))
* **composer:** update name and type ([3f93fc4](https://git.dropfort.com///commit/3f93fc4da82e022808c55630f966abfbe3f0a17e))
* **core:** allow d9 ([ba47187](https://git.dropfort.com///commit/ba47187b3bff0c8d153502e3963d1e7abf52ef25))
* **dependencies:** decrease drush dependency to 9.x ([fd7de81](https://git.dropfort.com///commit/fd7de819097678a47bbbea03e6b82d003986ec1b))
* **dependencies:** remove module upgrader ([3cdd22f](https://git.dropfort.com///commit/3cdd22fbe5a0f27db1f4d72d3dd57902c5c4d897))
* **dpm:** remove debug ([41cc680](https://git.dropfort.com///commit/41cc680e5f3a6e0905991a3b073642929d7d5d34))
* **drush:** allow either version ([60546ae](https://git.dropfort.com///commit/60546ae0f76c1834cb0c813b36a7fcbc072c0bdf))
* **form:** only set auth token if provided ([402e8b7](https://git.dropfort.com///commit/402e8b75e3856e60112e1c04f18e71c47efe2e9f))
* **install:** Site key registration ([1248791](https://git.dropfort.com///commit/12487917e710e30c451cbe733b78825eb8d627d4))
* **logging:** Ensure log update data errors ([1874e10](https://git.dropfort.com///commit/1874e1036d0c7ae43e5afcc0acb4422de2398ada))
* **make:** pass drush args through to pm-download ([069805e](https://git.dropfort.com///commit/069805ec859d7759d460c3fbfa07a97d56c4b4b6))
* **memory:** Fix memory bug ([21178a0](https://git.dropfort.com///commit/21178a0d54a184de437d554989dfeeee9b88bd7d))
* **options:** pass dropfort options through to pm-download ([0f895ea](https://git.dropfort.com///commit/0f895ea6c48132d6cdc4030ec8c4925c64d0f07e))
* **practices:** drupal best practice fixes ([7d312b7](https://git.dropfort.com///commit/7d312b7485b39039ef6e134932e60f303553a665))
* **practices:** more coding practice fixes ([bec28d8](https://git.dropfort.com///commit/bec28d84a795371a0cc3d57adac24c0f8069d0aa))
* **request:** update request methds ([4c8e63f](https://git.dropfort.com///commit/4c8e63f847d9ce44a75ff060bb394a9147f8650e))
* **security:** hide password value ([4c86b42](https://git.dropfort.com///commit/4c86b4257e3403840123f845ac41bce6c5cccb49))
* **sql:** Update column name ([4747823](https://git.dropfort.com///commit/4747823e92588d441c622172f560c1e64317f633))
* **sql:** Update column name ([b1b9ae2](https://git.dropfort.com///commit/b1b9ae27e8e37d5ca405dfa6adbc93b03839fe76))
* **standards:** apply autofixes ([30fa078](https://git.dropfort.com///commit/30fa078561064f209e787a2b3cdc33f60b010f03))
* **standards:** manual fixes ([15c94a3](https://git.dropfort.com///commit/15c94a365c2bb765dbd5f2992a80c663fdfed9a1))
* **standards:** manual fixes ([61e6fd7](https://git.dropfort.com///commit/61e6fd73960c537153912378d953453365924dd7))
* **standards:** replace t() ([787d1a1](https://git.dropfort.com///commit/787d1a1db2e8438c71189963b7e5cecc6e5c31ac))
* **status:** Always send site status ([ade8dc4](https://git.dropfort.com///commit/ade8dc41ff15c655301bbd4756a25001423de79d))
* **status:** Logic on status callback ([625d3c1](https://git.dropfort.com///commit/625d3c1f94ff02d831fad621c1a75f4a0d046080))
* **status:** skip sending if config isn't setup ([abf7f59](https://git.dropfort.com///commit/abf7f59948c29f3b8f2fdb3df32b8986c81341dc))
* **typo:** Fix grammar ([f3caba7](https://git.dropfort.com///commit/f3caba76f13e92b3daf8a61989c5743ac76dce9d))
* **update:** Fix hook name ([54ab820](https://git.dropfort.com///commit/54ab820494ce9e18673cba89322cd8397c4353e2))
* **update:** Fix update data refresh ([bd5661a](https://git.dropfort.com///commit/bd5661abcca7feae70119a114a5eda0a76438947))
* **update:** Send new data when module is enabled ([31429af](https://git.dropfort.com///commit/31429afa17fe05146ed7d3a75bfcd9331daacedc))
* **url:** add validation ([9044d3a](https://git.dropfort.com///commit/9044d3a70ef179d2475df00f9e298748dc255ca2))
